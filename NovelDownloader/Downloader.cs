﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace NovelDownloader
{
    class Downloader
    {
        /*
         * First page attribute is used to analyze book info,
         * All analyzer is based on desktop version of web page
         * 
         */
        public string FirstPage
        {
            get
            {
                if (string.IsNullOrEmpty(_firstPage))
                {
                    _firstPage = _Fetch(
                        _urlData.GetFirstPageUrl(), 
                        "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0", 
                        1);
                }
                return _firstPage;
            }
        }

        private UrlData _urlData;
        private string _firstPage;

        public Downloader(UrlData url)
        {
            _urlData = url;
            _firstPage = string.Empty;
        }

        /*
         * Download web page and uses Mobile user agent
         */
        public string Fetch(int page)
        {
            return _Fetch(_urlData.ToString(page), getUserAgent(page), page);
        }

        private string _Fetch(string url, string userAgent, int page)
        {
            string data;
            WebRequest httpRequest = WebRequest.Create(url);
            ((HttpWebRequest)httpRequest).UserAgent = userAgent;
            WebResponse webResponse = httpRequest.GetResponse();
            Stream responseStream = webResponse.GetResponseStream();
            StreamReader dataStream = new StreamReader(responseStream);                        
            
            data = dataStream.ReadToEnd();
            
            // clean up
            responseStream.Close();

            // debug
#if DEBUG
            using (StreamWriter outfile = new StreamWriter(String.Format("D:\\{0}-{1}-{2}.txt", _urlData.Host, _urlData.Tid, page)))
            {
                outfile.Write(data);
            }        
#endif

            return data;
        }

        private string getUserAgent(int i)
        {
            switch (i % 5)
            {
            case 0:
                return "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334";
            case 1:
                return "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A405";
            case 2:
                return "Mozilla/5.0 (Linux; Android 4.2.2; Nexus 7 Build/JDQ39) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166  Safari/535.19";
            case 3:
                return "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; SAMSUNG; OMNIA7)";
            default:
                return "Mozilla/5.0 (Linux; U; Android 2.3.4; fr-fr; HTC Desire Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
            }
        }
    }
}
