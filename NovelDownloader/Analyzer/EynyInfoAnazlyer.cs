﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovelDownloader.Analyzer
{
    public class EynyInfoAnazlyer : BaseInfoAnalyzer
    {
        public override string GetAuthor()
        {
            return Util.extractValue(_titleLine, @">\s?(?<author>\S*)", "author");
        }

        public override string GetTitle()
        {
            return Util.extractValue(_titleLine, @"-\s?[【《](?<title>[^】》]*)", "title");
        }

        public override int GetPageCount()
        {
            bool isPageArea = false;
            int count = 0;

            if (_pageLine != null)
                return base.GetPageCount();

            foreach (string line in _data.Split('\n'))
            {
                if (isPageArea && line.Contains("</div>"))
                {
                    count = Math.Max(GetValue(line), count);
                    break;
                }
                else if (isPageArea || line.Contains("class=\"page\""))
                {
                    count = Math.Max(GetValue(line), count);
                    isPageArea = true;
                }
            }

            return count;
        }

        public override string ExtractContent(string data)
        {
            string[] lines = data.Split('\r');
            StringBuilder sb = new StringBuilder();
            bool isContent = false;

            // always skip first line
            foreach (string line in lines)
            {
                if (isContent && line.Contains("class=\"page\""))
                {
                    break;
                }
                else if (isContent && line.Contains("/div&gt;"))
                {
                    int pos = 0;
                    if (line.Contains("class=\"author\""))
                    {
                        pos = (line.IndexOf("</h3>") != -1) ? line.IndexOf("</h3>") : 0;
                        isContent = true;
                    }
                    else
                    {
                        isContent = false;
                    }
                    sb.Append(Util.normalizeStr(line.Substring(pos)));
                    sb.Append(Environment.NewLine);
                }
                else if (line.Contains("class=\"author\"") || isContent)
                {
                    int pos;
                    pos = line.IndexOf("class=\"author\"");
                    if (pos != -1)
                    {
                        pos = line.IndexOf('>', pos) + 1;
                    }
                    else
                    {
                        // if content is true
                        pos = 0;
                    }
                    sb.Append(Util.normalizeStr(line.Substring(pos)));
                    isContent = true;
                }
                else
                {
                    isContent = false;
                }
            }

            return sb.ToString();
        }

        private int GetValue(string data)
        {
            int temp = 0;

            try
            {
                temp = int.Parse(Util.extractValue(data, @"-(?<page>\d+)\.", "page"));
            }
            catch (Exception ex)
            {
            }

            return temp;
        }
    }
}
