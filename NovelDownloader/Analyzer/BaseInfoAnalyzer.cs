﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovelDownloader.Analyzer
{
    public class BaseInfoAnalyzer
    {
        protected string _data;
        protected string _titleLine;
        protected string _pageLine;

        public void Init(string data)
        {
            _data = data;

            foreach (string line in _data.Split('\n'))
            {
                if (line.Contains("<title>"))
                {
                    _titleLine = line.Substring(line.IndexOf("<title>"), line.IndexOf("</title>"));
                }
                else if (line.Contains("class=\"pg\""))
                {
                    _pageLine = line;
                }

                if (!string.IsNullOrEmpty(_titleLine) &&
                    !string.IsNullOrEmpty(_pageLine))
                {
                    break;
                }
            } 
        }

        public virtual string GetAuthor()
        {
            throw new NotImplementedException("Not implemented: GetAuthor");
        }

        public virtual string GetTitle()
        {
            throw new NotImplementedException("Not implemented: GetTitle");
        }

        public virtual int GetPageCount()
        {
            int count = 0;
            string[] indexs = Util.extractValues(_pageLine, @"-\d*-(?<page>\d*)-", "page"); ;
            foreach (string index in indexs)
            {
                int value = int.Parse(index);
                if (count < value)
                {
                    count = value;
                }
            }

            return count;
        }

        public virtual string ExtractContent(string data)
        {
            string[] lines = data.Split('\n');
            StringBuilder sb = new StringBuilder();
            bool isContent = false;

            foreach (string line in lines)
            {
                if (isContent && line.Contains("</div>"))
                {
                    sb.Append(Environment.NewLine);
                    sb.Append(Util.normalizeStr(line));
                    sb.Append(Environment.NewLine);
                    isContent = false;
                }
                else if (line.Contains("postmessage_") || isContent)
                {
                    sb.Append(Util.normalizeStr(line));
                    isContent = true;
                }
            }

            return sb.ToString();
        }
    }
}
