﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovelDownloader.Analyzer
{
    public class InfoAnalyzerFactory
    {
        public static BaseInfoAnalyzer create(string host)
        {
            if (host.Contains("ck101"))
                return new Ck101InfoAnalyzer();
            else if (host.Contains("eyny"))
                return new EynyInfoAnazlyer();

            return null;
        }
    }
}
