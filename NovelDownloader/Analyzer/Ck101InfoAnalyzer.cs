﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovelDownloader.Analyzer
{
    public class Ck101InfoAnalyzer : BaseInfoAnalyzer
    {
        public override string GetAuthor()
        {
            return Util.extractValue(_titleLine, @"作.?者[:：]\s?(?<author>[^ （(]*)", "author");
        }

        public override string GetTitle()
        {
            return Util.extractValue(_titleLine, @">[^\]]*[\]]?\s?[【《]?(?<title>[^ （(】》]*)", "title");
        }
    }
}
