﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NovelDownloader
{
    class Util
    {
        private static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        public static string extractValue(string source, string pattern, string key)
        {
            Regex regex = new Regex(pattern);

            MatchCollection matches = regex.Matches(source);
            foreach (Match match in matches)
            {
                GroupCollection groups = match.Groups;
                return groups[key].Value.Trim();
            }

            return null;
        }

        public static string[] extractValues(string source, string pattern, string key)
        {
            int i = 0;
            string[] values = null;
            Regex regex = new Regex(pattern);

            MatchCollection matches = regex.Matches(source);
            values = new string[matches.Count];
            foreach (Match match in matches)
            {
                GroupCollection groups = match.Groups;
                values[i++] = groups[key].Value.Trim();
            }

            return values;
        }

        public static string removeHtml(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        public static string normalizeStr(string source)
        {
            string result = String.Empty;

            source = source.Trim();
            source = Regex.Replace(source, "<br.?/>", Environment.NewLine, RegexOptions.Compiled);
            source = Regex.Replace(source, "&nbsp;", "", RegexOptions.Compiled);
            result = removeHtml(source);
            if (result != Environment.NewLine)
                result = result.TrimStart();
            return result;
        }

        public static string getAppPath()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            return path.Substring(0, path.LastIndexOf('\\') + 1);
        }
    }
}
