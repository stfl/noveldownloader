﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NovelDownloader
{
    public partial class FrmMain : Form
    {
        public const string APP_NAME = "Novel Downloader";
        public const string VERSION = "v1.1";

        private Book _book;

        public FrmMain()
        {
            InitializeComponent();
            _book = null;

            // setting prog version
            this.Text = APP_NAME;
            lblVersion.Text = VERSION;
        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            try
            {
                btnDownload.Enabled = false;
                _book = new Book(txtUrl.Text, updateStatus);
                _book.GetInfo();
            }
            catch (Exception ex)
            {
                btnDownload.Enabled = true;
                updateStatus(_book, ex.Message);
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (_book == null)
            {
                MessageBox.Show("請輸入網址後先進行分析！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // update book page count
            try
            {
                _book.PageCount = Int32.Parse(txtEndPage.Text);
            }
            catch (Exception ex)
            {
                updateStatus("結束頁碼格式錯誤");
                txtEndPage.Text = "";
                return;
            }

            // update book title, author
            _book.Title = txtTitle.Text;
            _book.Author = txtAuthor.Text;

            if (!_book.IsFetching)
            {
                _book.Fetch();
                btnDownload.Text = "取消下載";
            }
            else
            {
                _book.AbortFetch();
                btnDownload.Text = "下載";
            }
        }

        private void updateStatus(params object[] objs)
        {
            foreach (object obj in objs)
            {
                if (obj is Book)
                {
                    Book book = (Book)obj;
                    if (txtAuthor.InvokeRequired)
                    {
                        txtAuthor.Invoke(new UIDelegate.UpdateStatus(updateStatus), new object[] { new object[] { book } });
                    }
                    else
                    {
                        txtAuthor.Text = (book.Author == null ? "" : book.Author);
                        txtTitle.Text = (book.Title == null ? "" : book.Title);
                        txtEndPage.Text = (book.PageCount == 0 ? "" : book.PageCount.ToString());
                        btnDownload.Enabled = true;
                        btnDownload.Text = "下載";

                                                if (!_book.IsValid())
                        {
                            _book = null;
                        }
                    }
                }
                else if (obj is string)
                {
                    string msg = (string)obj;
                    if (lblStatus.InvokeRequired)
                    {
                        lblStatus.Invoke(new UIDelegate.UpdateStatus(updateStatus), new object[] { new object[] { obj } });
                    }
                    else
                    {
                        lblStatus.Text = "目前狀態：" + msg;
                    }
                }
            }
        }

        private void lblVersion_DoubleClick(object sender, EventArgs e)
        {
            string title = String.Format("{0} {1}",
                APP_NAME,
                VERSION);

            MessageBox.Show("Author: BlakeY <stfl0622@gmail.com>", title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
