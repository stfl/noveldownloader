﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovelDownloader
{
    class UrlData
    {
        // attribute
        public string Host
        {
            get
            {
                return _host;
            }
        }

        public string Tid
        {
            get
            {
                return _tid;
            }
        }

        // varible
        private string _host;
        private string _tid;

        public UrlData(string url)
        {
            if (!getHost(url))
            {
                throw new InvalidOperationException("Unable to verify URL: Host");
            }
            if (!getTid(url))
            {
                throw new InvalidOperationException("Unable to verify URL: Tid");
            }
        }

        private bool getHost(string url)
        {
            _host = Util.extractValue(url, @"http://(?<domain>[A-Za-z0-9.]*)/", "domain");

            return _host == null ? false : true;
        }

        private bool getTid(string url)
        {
            _tid = Util.extractValue(url, @"thread-(?<tid>\d*)-", "tid");

            if (_tid == null)
            {
                _tid = Util.extractValue(url, @"tid-(?<tid>\d*)", "tid");
            }

            return _tid == null ? false : true;
        }

        public string GetFirstPageUrl()
        {
            return string.Format("http://{0}/thread-{1}-1-1.html", _host, _tid);
        }

        public string ToString(int page)
        {
            if (_host.Contains("eyny"))
            {
                // hxxp://www.eyny.com/archiver/tid-<tid>-<page>.html
                return string.Format("http://{0}/archiver/tid-{1}-{2}.html", _host, _tid, page);
            }

            return string.Format("http://{0}/thread-{1}-{2}-1.html", _host, _tid, page);
        }

        public override string ToString()
        {
            return ToString(1);
        }
    }
}
