﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using NovelDownloader.Analyzer;

namespace NovelDownloader
{
    class Book
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public int PageCount { get; set; }
        public bool IsFetching
        {
            get
            {
                return _isFetching;
            }
        }

        private Downloader _downloader;
        private UrlData _urlData;
        private string[] _pages;
        private UIDelegate.UpdateStatus _updateStatusHandler;
        private bool _isFetching;
        private Signal _signal;
        private BaseInfoAnalyzer _analyzer;

        public Book(string url, UIDelegate.UpdateStatus handler)
        {
            try
            {
                _urlData = new UrlData(url);
                _downloader = new Downloader(_urlData);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException("Invalid URL Address");
            }

            Author = null;
            Title = null;
            PageCount = 0;
            _pages = null;
            _updateStatusHandler = handler;
            _isFetching = false;
            _signal = null;
            _analyzer = null;
        }

        public void GetInfo()
        {
            Thread thread = new Thread(_GetInfo);
            thread.Start();
        }

        public void _GetInfo()
        {
            string infoLine = string.Empty;
            string pageLine = string.Empty;

            _updateStatusHandler("分析小說資料中...");
            _analyzer = InfoAnalyzerFactory.create(_urlData.Host);

            if (_analyzer == null)
            {
                _updateStatusHandler(this, "不支援此網址！");
                return;
            }
            else
            {
                _analyzer.Init(_downloader.FirstPage);
            }
            Author = _analyzer.GetAuthor();
            Title = _analyzer.GetTitle();
            PageCount = _analyzer.GetPageCount();

            if (string.IsNullOrEmpty(Author) || string.IsNullOrEmpty(Title) || (PageCount == 0))
            {
                _updateStatusHandler(this, "無法順利取得小說資訊，請手動輸入！");
                return;
            }

            // update book info to UI
            _updateStatusHandler(this, "已更新小說資訊");
        }

        public bool Fetch()
        {
            bool ret = false;

            _signal = new Signal();

            Thread fetchManager = new Thread(StartFetchManager);
            fetchManager.Start(_signal);

            return ret;
        }

        public void AbortFetch()
        {
            _isFetching = false;

            if (_signal != null)
            {
                _signal.FireStop();
                _updateStatusHandler("使用者中斷下載");
            }
        }

        public bool IsValid()
        {
            return !(string.IsNullOrEmpty(Author) && string.IsNullOrEmpty(Title) && (PageCount == 0));
        }

        public override string ToString()
        {
            return String.Format("Title {0}, Author: {1}, PageCount: {2}", Title, Author, PageCount);
        }

        private bool Cook(long elapsedMilliseconds)
        {
            StreamWriter file;
            string fileName = String.Format("{0}{1}.txt", Util.getAppPath(), Title);

            if (_pages == null)
            {
                _updateStatusHandler("小說下載失敗，請重試");
                return false;
            }

            file = new StreamWriter(fileName);
            foreach (string page in _pages)
            {
                if (String.IsNullOrEmpty(page))
                {
                    file.Close();
                    File.Delete(fileName);
                    _updateStatusHandler("小說下載失敗，請重試");
                    return false;
                }
                file.Write(page);
            }

            file.Close();
            _updateStatusHandler(this, String.Format("小說下載完成 (費時：{0} 秒)", elapsedMilliseconds/1000.0));
            return true;
        }

        private void StartFetchManager(object param)
        {
            const int MAX_WORKERS = 5;
            Stopwatch stopwatch = new Stopwatch();
            int count = 0, j, max;
            Thread[] workers;
            _signal = (Signal)param;
            _isFetching = true;

            stopwatch.Start();
            // prepare storage
            _pages = new string[PageCount];
            while (count < PageCount)
            {
                if (_signal.IsStop())
                {
                    return;
                }
                
                _updateStatusHandler(String.Format("下載第 {0} 頁之後的內容...", count + 1));

                max = ((count + MAX_WORKERS) < PageCount ? MAX_WORKERS : PageCount - count);                

                workers = new Thread[max];
                for (j = 0; j < max; j++)
                {
                    workers[j] = new Thread(StartWorker);
                    workers[j].Name = string.Format("Page-{0}", count + j + 1);
                    workers[j].IsBackground = true;
                    workers[j].Start(count + j);
                }

                for (j = 0; j < max; j++)
                {
                    workers[j].Join();
                }

                count = count + max;
            }
            stopwatch.Stop();

            _isFetching = false;
            Cook(stopwatch.ElapsedMilliseconds);
        }

        private void StartWorker(object param)
        {
            int page = (int)param + 1;

            if (_analyzer != null)
            {
                _pages[page - 1] = _analyzer.ExtractContent(_downloader.Fetch(page));
            }
        }

        private void updateStatus(string msg)
        {
            if (_updateStatusHandler != null)
            {
                _updateStatusHandler(msg);
            }
        }
    }

    class Signal
    {
        private volatile bool _stop;

        public Signal()
        {
            _stop = false;
        }

        public void FireStop()
        {
            _stop = true;
        }

        public bool IsStop()
        {
            return _stop;
        }
    }
}
